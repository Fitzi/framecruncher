#include <iostream>
#include <opencv2/opencv.hpp>

#ifndef FRAMECRUNCHER_VIDEO_H
#define FRAMECRUNCHER_VIDEO_H

#include "Frame.h"

using namespace cv;
using namespace std;

class Video {

protected:
    string path;

    unsigned long length;
    unsigned long currentFrame;

    VideoCapture* videoCapture;

public:
    Video( string path );
    ~Video();

    bool load();
    unsigned long getLength();
    Frame* getNextFrame();
};


#endif