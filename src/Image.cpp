#include "Image.h"

Image::Image(unsigned int width, unsigned int height) {
    this->width = width;
    this->height = height;
    this->image = new Mat(height, width, CV_8UC3, Scalar(0,0,0));
}

Image::~Image(){
    delete this->image;
}

void Image::drawFrame( Frame* frame ){
    Point p1(frame->getIndex(), 0);
    Point p2(frame->getIndex(), this->height);
    line( *this->image, p1, p2, frame->getDominantColor() );
}

void Image::save(string filename) {
    imwrite(filename, *this->image);
}

void Image::display( string windowName ){
    imshow( windowName, *this->image );
}