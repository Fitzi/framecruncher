#include "Frame.h"

Frame::Frame(unsigned long index) {
    this->index = index;
    this->dominantColor = Vec3b(0,0,0);
    this->frame = new Mat();
}

Frame::~Frame(){
    delete this->frame;
}

unsigned long Frame::getIndex(){
    return this->index;
}

void Frame::analyze() {

    Mat samples(this->frame->rows * this->frame->cols, 3, CV_32F);
    for (int y = 0; y < this->frame->rows; y++)
        for (int x = 0; x < this->frame->cols; x++)
            for (int z = 0; z < 3; z++)
                samples.at<float>(y + x * this->frame->rows, z) = this->frame->at<Vec3b>(y, x)[z];


    int clusterCount = 1;
    Mat labels;
    int attempts = 5;
    Mat centers;
    kmeans(samples, clusterCount, labels, TermCriteria(CV_TERMCRIT_ITER | CV_TERMCRIT_EPS, 10000, 0.0001), attempts,
           KMEANS_PP_CENTERS, centers);

    Vec3b result;
    int cluster_idx = labels.at<int>(0,0);
    result[0] = centers.at<float>(cluster_idx, 0);
    result[1] = centers.at<float>(cluster_idx, 1);
    result[2] = centers.at<float>(cluster_idx, 2);

    this->dominantColor = result;

}

Vec3b Frame::getDominantColor(){
    return this->dominantColor;
}