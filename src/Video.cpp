#include <opencv2/opencv.hpp>
#include "Video.h"

using namespace cv;
using namespace std;

Video::Video(string path) {
    this->path = path;
    this->currentFrame = 0;
    this->length = 0;
    this->videoCapture = nullptr;
}

Video::~Video() {
    delete this->videoCapture;
}

bool Video::load() {

    // TODO check if video actually exists and can be loaded

    this->videoCapture = new VideoCapture(this->path);

    if (!this->videoCapture->isOpened()) {
        cout << "VideoCapture could not be opened" << endl;
        return false;
    }

    this->length = (unsigned long) this->videoCapture->get(CAP_PROP_FRAME_COUNT);

    return true;
}

unsigned long Video::getLength() {
    return this->length;
}

Frame *Video::getNextFrame() {

    if (currentFrame >= this->length) {
        return nullptr;
    }

    auto *frame = new Frame(this->currentFrame);

    *this->videoCapture >> *frame->frame;
    this->currentFrame++;

    return frame;
}