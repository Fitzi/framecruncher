#include <opencv2/opencv.hpp>
#include <opencv2/videoio.hpp>
#include <string>
#include "Frame.h"

#ifndef FRAMECRUNCHER_IMAGE_H
#define FRAMECRUNCHER_IMAGE_H

using namespace cv;
using namespace std;

class Image {
protected:
    unsigned int width;
    unsigned int height;
    Mat* image;

public:
    Image( unsigned int width, unsigned int height );
    ~Image();

    void drawFrame( Frame* frame );

    void save(string filename);
    void display( string windowName );
};


#endif