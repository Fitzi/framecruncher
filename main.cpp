#include <iostream>
#include "src/FrameCruncher.h"

using namespace std;


int main(int, char **) {

    string fileName("../testdata/FindingDoryTrailer.mp4");
    string windowName("Frame Cruncher");

    FrameCruncher cruncher(fileName, windowName);


    cruncher.startCrunching();


    return 0;
}