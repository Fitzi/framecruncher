cmake_minimum_required(VERSION 3.5)
project(FrameCruncher)

set(CMAKE_CXX_STANDARD 11)
set (CMAKE_CXX_FLAGS "-pthread")

find_package(OpenCV REQUIRED)

include_directories( ${OpenCV_INCLUDE_DIRS} )

add_executable(FrameCruncher main.cpp src/FrameCruncher.cpp src/FrameCruncher.h src/Video.cpp src/Video.h src/Image.cpp src/Image.h src/Frame.cpp src/Frame.h)

target_link_libraries(FrameCruncher ${OpenCV_LIBS})